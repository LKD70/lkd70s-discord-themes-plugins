//META{"name":"SuppressRoleMentions","website":"https://gitlab.com/LKD70/suppressrolementions","source":"https://gitlab.com/LKD70/suppressrolementions"}*//

class SuppressRoleMentions {

	getName() { return "SuppressRoleMentions"; }
	getDescription() { return "Suppresses @role mentions"; }
	getVersion() { return "0.1.0"; }
	getAuthor() { return "LKD70"; }
	getChanges() {
		return {

		};
	}

	load() {} // Legacy

	start() {
		const libLoadedEvent = () => {
			try{ this.onLibLoaded(); }
			catch(err) { console.error(this.getName(), "fatal error, plugin could not be started!", err); try { this.stop(); } catch(err) { console.error(this.getName() + ".stop()", err); } }
		};

		let lib = document.getElementById("NeatoBurritoLibrary");
		if (!lib) {
			lib = document.createElement("script");
			lib.id = "NeatoBurritoLibrary";
			lib.type = "text/javascript";
			lib.src = "";
			document.head.appendChild(lib);
		}

		this.forceLoadTimeout = setTimeout(libLoadedEvent, 30000);
		if (typeof window.NeatoLib !== "undefined") libLoadedEvent();
		else lib.addEventListener("load", libLoadedEvent);
	}

	get settingFields() {
		return {
			suppressedRoles: { title: "Suppressed role @tags", description: "You can also add and remove suppressed roles from the context menu", type: "string", array: true }
		};
	}

	get defaultSettings() {
		return {
			displayUpdateNotes: true,
			suppressedRoles: ["410965084546793472"]
		};
	}

	getSettingsPanel() {
		return NeatoLib.Settings.createPanel(this);
	}

	saveSettings() {
		NeatoLib.Settings.save(this);
	}

	onLibLoaded() {
		this.settings = NeatoLib.Settings.load(this);

		NeatoLib.Updates.check(this);

		this.unpatch = NeatoLib.monkeyPatchInternal(NeatoLib.Modules.get("isMentioned"), "isMentioned", e => {
			if (e.args[0].mention_roles.some(m => this.settings.suppressedRoles.includes(m))) return false;
			else return e.callDefault();
		});

		document.addEventListener("contextmenu", this.contextEvent = e => this.onContextMenu(e));

		NeatoLib.Events.onPluginLoaded(this);
	}

	onContextMenu(e) {
		if (!e.target.className.includes("username") && !e.target.className.includes("large")) return;

		const uid = NeatoLib.ReactData.getProp(NeatoLib.DOM.searchForParentElementByClassName(e.target, NeatoLib.getClass("containerCozy", "container")), "messages.0.author.id");
		if (!uid) return;

		const contextMenu = NeatoLib.ContextMenu.get();
		if (!contextMenu) return;

		contextMenu.insertAdjacentElement("afterBegin", NeatoLib.ContextMenu.createGroup([
			!this.settings.suppressedRoles.includes(uid) ? NeatoLib.ContextMenu.createItem("Suppress User Mentions", () => {
				this.settings.suppressedRoles.push(uid);
				this.saveSettings();
				NeatoLib.ContextMenu.close();
			}) :
			NeatoLib.ContextMenu.createItem("Unsuppress User Mentions", () => {
				this.settings.suppressedRoles.splice(this.settings.suppressedRoles.indexOf(uid), 1);
				this.saveSettings();
				NeatoLib.ContextMenu.close();
			})
		]));
	}

	stop() {
		this.unpatch();
		document.removeEventListener("contextmenu", this.contextEvent);
	}

}
