# LKD70's Discord themes & plugins
A collection of LKD70's plugins and themes for Discod. These should work with most BD based injector/patchers.

## Contents
### [Plugins](/plugins/)

* [Suppress Role Mentions](/plugins/SuppressRoleMentions.plugin.js) - [Info](#suppress-role-mentions)

### [Themes](/themes/)

* [LKDarker Discord](/themes/LKDarkerDiscord.theme.css) - [Info](#lkdarker-discord)
* [No Gift Button](/themes/NoGiftButton.theme.css)
* [No Social Links](/themes/NoSocialLinks.theme.css)
* [Square Servers](/themes/SquareServers.theme.css)
* [Theme Scrollbar](/themes/ThemeScrollbar.theme.css)
* [Members Popout](/themes/membersPopout.theme.css)

## Additional information

### Suppress Role Mentions

This is a fork of [SuppressUserMentions](https://github.com/Metalloriff/BetterDiscordPlugins/blob/master/SuppressUserMentions.plugin.js) by Metalloriff. All credit goes to him.

This plugin allows you to suppress mentions for specific roles for all servers. 

The roles can be added using the role ID from the settings page.


### LKDarker Discord

This is the base theme, it can be used without any of the other modular themes,
but it is recommended you at least add the [Theme Scrollbar](#theme-scrollbar) theme with this.

The theme features a couple root variables for changing all the colours of the theme.
The aim of this theme is to keep it flat, flowing, and most importantly, dark.

Settings can be edited on the fly using the [ThemeSettings](https://github.com/mwittrien/BetterDiscordAddons/tree/master/Plugins/ThemeSettings) plugin by DevilBro.


### No Gift Button

This small theme patch simply removes the gift button from the message input field.
This can also be achived through Experiments I think, but I'm too lazy to check.

### No Social Links

This will remove all discord social links

### Square Servers

This makes Discord servers appear square, shaded, and highlighted when active.
For those of us who don't like wasted space with rounded images.

### Theme Scrollbar

This will add a custom colour option to the scrollbar.

The theme features a couple root variables for changing all the colours of the theme.

Settings can be edited on the fly using the [ThemeSettings](https://github.com/mwittrien/BetterDiscordAddons/tree/master/Plugins/ThemeSettings) plugin by DevilBro.


### Members Popout

Adds a popout effect to the members panel. Allows you to see members icons by default,
and thier icon + name on hover. 